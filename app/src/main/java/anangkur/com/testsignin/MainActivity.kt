package anangkur.com.testsignin

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import com.facebook.CallbackManager
import com.facebook.login.widget.LoginButton
import com.facebook.FacebookException
import com.facebook.login.LoginResult
import com.facebook.FacebookCallback
import android.content.Intent
import android.util.Log
import android.widget.Toast
import com.facebook.GraphRequest
import com.google.android.gms.auth.api.signin.GoogleSignIn
import com.google.android.gms.auth.api.signin.GoogleSignInClient
import com.google.android.gms.auth.api.signin.GoogleSignInOptions
import com.google.android.gms.common.SignInButton
import com.google.android.gms.auth.api.signin.GoogleSignInAccount
import com.google.android.gms.common.api.ApiException
import com.google.android.gms.tasks.Task
import com.google.android.gms.tasks.OnCompleteListener


class MainActivity : AppCompatActivity() {

    lateinit var loginButton: LoginButton
    lateinit var signInButton: SignInButton
    lateinit var callbackManager: CallbackManager
    lateinit var mGoogleSignInClient: GoogleSignInClient
    var account: GoogleSignInAccount? = null

    val RC_SIGN_IN = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        defineViews()
        facebookLogin()
        googleSignin()
    }

    private fun defineViews(){
        loginButton = findViewById<LoginButton>(R.id.login_button)
        signInButton = findViewById<SignInButton>(R.id.sign_in_button)
        signInButton.setOnClickListener {
            account = GoogleSignIn.getLastSignedInAccount(this);
            if (account == null){
                signIn()
            }else{
                signOut()
            }
        }
    }

    private fun facebookLogin(){
        callbackManager = CallbackManager.Factory.create();
        // If you are using in a fragment, call loginButton.setFragment(this);

        // Callback registration
        loginButton.registerCallback(callbackManager, object : FacebookCallback<LoginResult> {
            override fun onSuccess(loginResult: LoginResult) {
                val request = GraphRequest.newMeRequest(loginResult.accessToken) { `object`, response ->
                    Log.v("LoginActivity", response.toString())

                    // Application code
                    val id = `object`.getString("id")
                    val name = `object`.getString("name")
                    val email = `object`.getString("email")
                    val birthday = `object`.getString("birthday") // 01/31/1980 format
                    val profilePhoto = getFacebookProfilePicture(id)
                    println("name: ${name}")
                    println("id: ${id}")
                    println("email: ${email}")
                    println("birthday: ${birthday}")
                    println("profilePhoto: ${profilePhoto}")
                    Toast.makeText(this@MainActivity, "login berhasil!", Toast.LENGTH_SHORT).show()
                }
                val parameters = Bundle()
                parameters.putString("fields", "id,name,email,gender,birthday")
                request.parameters = parameters
                request.executeAsync()
            }

            override fun onCancel() {
                Toast.makeText(this@MainActivity, "login cancel", Toast.LENGTH_SHORT).show()
            }

            override fun onError(exception: FacebookException) {
                Toast.makeText(this@MainActivity, "login error", Toast.LENGTH_SHORT).show()
                println("error: ${exception.printStackTrace()}")
            }
        })
    }

    private fun getFacebookProfilePicture(userID: String): String {
        return "https://graph.facebook.com/$userID/picture?type=large"
    }

    private fun googleSignin(){
        // Configure sign-in to request the user's ID, email address, and basic
        // profile. ID and basic profile are included in DEFAULT_SIGN_IN.
        val gso = GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
            .requestEmail()
            .build()
        // Build a GoogleSignInClient with the options specified by gso.
        mGoogleSignInClient = GoogleSignIn.getClient(this, gso);
    }

    private fun signIn() {
        val signInIntent = mGoogleSignInClient.signInIntent
        startActivityForResult(signInIntent, RC_SIGN_IN)
    }

    private fun signOut() {
        mGoogleSignInClient.signOut()
            .addOnCompleteListener(this, object : OnCompleteListener<Void> {
                override fun onComplete(task: Task<Void>) {
                    revokeAccess()
                    Toast.makeText(this@MainActivity, "logout berhasil!", Toast.LENGTH_SHORT).show()
                }
            })
    }

    private fun revokeAccess() {
        mGoogleSignInClient.revokeAccess()
            .addOnCompleteListener(this) {
                // ...
            }
    }

    private fun handleSignInResult(completedTask: Task<GoogleSignInAccount>) {
        try {
            val account = completedTask.getResult(ApiException::class.java)
            println("name: ${account.displayName}")
            println("email: ${account.email}")
            println("photo: ${account.photoUrl}")
            Toast.makeText(this, "login berhasil!", Toast.LENGTH_SHORT).show()
        } catch (e: ApiException) {
            e.printStackTrace()
        }

    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        callbackManager.onActivityResult(requestCode, resultCode, data);

        // Result returned from launching the Intent from GoogleSignInClient.getSignInIntent(...);
        if (requestCode === RC_SIGN_IN) {
            // The Task returned from this call is always completed, no need to attach
            // a listener.
            val task = GoogleSignIn.getSignedInAccountFromIntent(data)
            handleSignInResult(task)
        }
    }
}
